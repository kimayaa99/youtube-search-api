﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace youtubesearchapi.Models
{
     
    public class YouTubeSearchModelList
    {
        
        public string Title { get; set; }
        public string VideoId { get; set; }
        public string VideoURL { get; set; }

        public List<YouTubeSearchModelList> youTubeSearchModelList { get; set; }

    }


}