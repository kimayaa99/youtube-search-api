﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(youtubesearchapi.Startup))]
namespace youtubesearchapi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
