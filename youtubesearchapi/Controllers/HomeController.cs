﻿using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.Util.Store;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Google.Apis.Customsearch.v1.Data;
using youtubesearchapi.Models;

namespace youtubesearchapi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string searchString)
        {
            if (searchString == null)
            {
                List<YouTubeSearchModelList> youTubeSearchList = new List<YouTubeSearchModelList>();
                return View("YouTubeSearchResults", youTubeSearchList);
            }
            else
            {
                return RedirectToAction("GetYouTubeSearchResults/" + searchString);
            }
           
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public async Task<ActionResult> GetYouTubeSearchResults(string searchString)

        {
            if(searchString !=null)
            {
                ViewBag.SearchString = searchString;
            }
            if (searchString == null && ViewBag.SearchString !=null)
            {
                searchString = ViewBag.SearchString;
                 
            }
            if (searchString == null && ViewBag.SearchString == null)
            {
                List<YouTubeSearchModelList> youTubeList = new List<YouTubeSearchModelList>();
                return View("YouTubeSearchResults", youTubeList);
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                ApiKey = "AIzaSyBw9romt_BADHLtDpnFa2RiQqaSMinKX6E",
                ApplicationName = this.GetType().ToString()
            });

            var searchListRequest = youtubeService.Search.List("snippet");
            searchListRequest.Q = searchString; //"django"; // Replace with your search term.
            searchListRequest.MaxResults = 50;
           
            // Call the search.list method to retrieve results matching the specified query term.
            var searchListResponse = await searchListRequest.ExecuteAsync();

            var videoURL = @"https://www.youtube.com/watch?v=";
            List<string> videos = new List<string>();

            List<YouTubeSearchModelList> youTubeSearchModelList = new List<YouTubeSearchModelList>();


            foreach (var searchResult in searchListResponse.Items)
            {
                switch (searchResult.Id.Kind)
                {
                    case "youtube#video":
                        videos.Add(String.Format("{0} ({1})", searchResult.Snippet.Title, searchResult.Id.VideoId));

                        youTubeSearchModelList.Add(new YouTubeSearchModelList() { Title = Convert.ToString(searchResult.Snippet.Title), VideoId = Convert.ToString(searchResult.Id.VideoId), VideoURL = Convert.ToString(videoURL + searchResult.Id.VideoId) });

                        break;


                }
            }

            return await Task.Run<ActionResult>(() =>
            {
                if (true)
                {
                  //  RedirectToAction("GetYouTubeSearchResults");
                    return View("YouTubeSearchResults", youTubeSearchModelList);
                }
                else
                {
                    List<YouTubeSearchModelList> youTubeSearchList = new List<YouTubeSearchModelList>();
                    return View("YouTubeSearchResults", youTubeSearchList);
                }
            });

        }
 
    }
}
    
 